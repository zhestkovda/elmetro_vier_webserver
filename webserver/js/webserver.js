'use strict';

let ViER_model;
let time; // 15833323339000
let timeUNIX; // 19-05-2018 20:44:12
let MAX_NUMBER_OF_CHANNELS;
const MAX_NUMBER_OF_CHANNELS_PER_PAGE = 16;
const MAX_NUMBER_OF_PAGES = 8;
let timerId;
let timestamps = [];
let final_datasets = [null,null,null,null,null,null,null,null]; //0...7 all plot in page 0
let datasets2plot = [];
let dataset_colors = ["#A0522D","#A0522D","#A0522D","#A0522D","#A0522D","#A0522D","#A0522D","#A0522D"];
let all_colors = {"#A0522D":"1",
				  "#CD5C5C":"2",
				  "#FF4500":"3",
				  "#008B8B":"4",
				  "#B8860B":"5",
				  "#32CD32":"6",
				  "#FFD700":"7",
				  "#48D1CC":"8",
				  "#87CEEB":"9",
				  "#FF69B4":"10",
				  "#CD5C5C":"11",
				  "#87CEFA":"12",
				  "#6495ED":"13",
				  "#DC143C":"14",
				  "#FF8C00":"15",
				  "#C71585":"16",
				  "#800080":"17",
				  "#FFFF00":"18",
				  "#000080":"19",
				  "#000000":"20"
}
const AI_TYPE = 'AI';
const AO_TYPE = 'AO';
const DI_TYPE = 'DI';
const DO_TYPE = 'DO';
const TTL_TYPE = 'TTL';
let tables = []; // array of all 1-8 datatablebody elements
let curTime;
let curData; 
let rownumber; //number of rows in table 0
let rowTemplate_0;
let rowTemplate_18;
//let tables; // array of all 1-8 datatablebody elements
let isGraphInit = false;
// structure
const RegConfig = {
  ConfigChangeCtr: null,
  Status: null,

  AIChannelInfo: null,
  AOChannelInfo: null,
  DIChannelInfo: null,
  DOChannelInfo: null,

  NameSignalsAll: [], // = NameSignalsAI + NameSignalsAO + NameSignalsDI + NameSignalsDO +NameSignalsTTL
  NameSignalsAI: [],
  NameSignalsAO: [],
  NameSignalsDI: [],
  NameSignalsDO: [],
  NameSignalsTTL: [],

  AIConfig: null,
  AOConfig: null,
  DIConfig: null,
  DOConfig: null,
  TTLConfig: null,
  DisplayConfig: null,

  AICount: null,
  AOCount: null,
  DICount: null,
  DOCount: null,
  TTLCount: null,

  DescSignalsAll: [],
  DescSignalsAI: [],
  DescSignalsAO: [],
  DescSignalsDI: [],
  DescSignalsDO: [],
  DescSignalsTTL: [],

  UnitSignalsAll: [],
  UnitSignalsAI: [],
  UnitSignalsTTL: []
};

let AIarray;
let AOarray;
let DIarray;
let DOarray;
let TTLarray;

let plot_options18 = []; 					//options for plots 1-8
let datasets18 = []; 					//datasets for signals 
let plot18 = [];						//plots for 1-8 pages
let legends18 = [];						//div legend area for plots 1-8
let flot_placeholders18 = [];	        //div 1-8 placeholders

// flot_placeholders18.push($('#flot-1-placeholder'));
// flot_placeholders18.push($('#flot-2-placeholder'));
// flot_placeholders18.push($('#flot-3-placeholder'));
// flot_placeholders18.push($('#flot-4-placeholder'));
// flot_placeholders18.push($('#flot-5-placeholder'));
// flot_placeholders18.push($('#flot-6-placeholder'));
// flot_placeholders18.push($('#flot-7-placeholder'));
// flot_placeholders18.push($('#flot-8-placeholder'));

// legends18.push($('#legend-1'));
// legends18.push($('#legend-2'));
// legends18.push($('#legend-3'));
// legends18.push($('#legend-4'));
// legends18.push($('#legend-5'));
// legends18.push($('#legend-6'));
// legends18.push($('#legend-7'));
// legends18.push($('#legend-8'));

let av18 = [
  {
    npi: [],
    vpi: [],
    description: [],
    units: [],
    labels: [],
    plotdata: [],
    pageDesc: ''
  },
  {
    npi: [],
    vpi: [],
    description: [],
    units: [],
    labels: [],
    plotdata: [],
    pageDesc: ''
  },
  {
    npi: [],
    vpi: [],
    description: [],
    units: [],
    labels: [],
    plotdata: [],
    pageDesc: ''
  },
  {
    npi: [],
    vpi: [],
    description: [],
    units: [],
    labels: [],
    plotdata: [],
    pageDesc: ''
  },
  {
    npi: [],
    vpi: [],
    description: [],
    units: [],
    labels: [],
    plotdata: [],
    pageDesc: ''
  },
  {
    npi: [],
    vpi: [],
    description: [],
    units: [],
    labels: [],
    plotdata: [],
    pageDesc: ''
  },
  {
    npi: [],
    vpi: [],
    description: [],
    units: [],
    labels: [],
    plotdata: [],
    pageDesc: ''
  },
  {
    npi: [],
    vpi: [],
    description: [],
    units: [],
    labels: [],
    plotdata: [],
    pageDesc: ''
  }
];
let plotdata = [];
let DatasetTemplate = {
  label: '',
  data: [],
  color: 0,
  points: { fillColor: '#FFFFFF', show: true },
  lines: { show: true }
};
let plot0 = null; // plot = $("#flot-0-placeholder"), final_datasets, plot_options
let plot_options0;

let TextSize; 
let PollPeriod; 
let ShowInterval; 
let ShowGraph;

$(document).ready(function() {	
  //alert('doc is ready');
  $('#btnplay').fadeTo(1, 0.5);
  $('#btnstop').fadeTo(1, 1.0);
 
  $('#btnplay').click(function() // START POLLING
  {
    timerId = setTimeout(getRegData, PollPeriod * 1000);
    $('#btnplay').fadeTo(1, 0.5);
    $('#btnstop').fadeTo(1, 1.0);
  });

  $('#btnstop').click(function() // STOP POLLING
  {
    clearTimeout(timerId);
    $('#btnplay').fadeTo(1, 1.0);
    $('#btnstop').fadeTo(1, 0.5);
  });

  $('#btnplay')
    .append('<img src=img/play.png />')
    .button();
  $('#btnstop')
    .append('<img src=img/stop.png />')
    .button();

  // redraw plot 0 when dblclick
  $('#flot-0-placeholder').dblclick(function() {
    plot0 = $.plot($('#flot-0-placeholder'), datasets2plot, plot_options0);
    plot0.draw();
  });

  // zooming plot 0
  $('#flot-0-placeholder').bind('plotselected', function(event, ranges) {
    $.each(plot0.getXAxes(), function(_, axis) {
      let opts = axis.options;
      opts.min = ranges.xaxis.from;
      opts.max = ranges.xaxis.to;
    });
    $.each(plot0.getYAxes(), function(_, axis) {
      let opts = axis.options;
      opts.min = ranges.yaxis.from;
      opts.max = ranges.yaxis.to;
    });
    plot0.setupGrid();
    plot0.draw();
    plot0.clearSelection();
  });

  // redraw plot18 when dblclick
  for (let j = 0; j < MAX_NUMBER_OF_CHANNELS; j++) {
  	if(flot_placeholders18[j]){
	  	  flot_placeholders18[j].forEach((flotph, i) => {
          flotph.dblclick(function() {
			      plot18[j][i] = $.plot(flotph, datasets18[j][i], plot_options18[j][i]);
				    plot18[j][i].setupGrid();
				    plot18[j][i].draw();
			   });
	  	});
    }
  }
}); // $(document).ready(function () {


async function fetchReg(url, options = {}) {
  if (!options.method) {
    options.method = 'GET';
  }
  if (!options.headers) {
    options.headers = {
      'Content-Type': 'application/json'
    };
  }
  return fetch(url, options).then(data => data.json());
}

async function getRegInfo() {
  // $.ajax({
  //   type: 'GET',
  //   async: true, // !!!by default
  //   url: '/RegInfo', //url: '/index.html?getRegInfo',
  //   dataType: 'json',
  //   success: handleRegInfo
  // });
  const data = await fetchReg('/RegInfo');
  handleRegInfo(data);
}

// обработка RegInfo
function handleRegInfo(data) {
  ViER_model = data['configuration'];

  if (typeof Storage !== 'undefined') {
    localStorage.setItem('ViER_model', ViER_model);
  }
  $('#configuration').append(data['configuration']);
  $('#plantnumber').append(data['serial']);
  $('#software').append(data['swversion']);

  // RegConfig = Object.assign({}, data.DeviceInfo);
  RegConfig.AICount = data.DeviceInfo.AICount;
  RegConfig.AOCount = data.DeviceInfo.AOCount;
  RegConfig.DICount = data.DeviceInfo.DICount;
  RegConfig.DOCount = data.DeviceInfo.DOCount;
  RegConfig.TTLCount = data.DeviceInfo.TTLCount;
  MAX_NUMBER_OF_CHANNELS =
    RegConfig.AICount +
    RegConfig.AOCount +
    RegConfig.DICount +
    RegConfig.DOCount +
    RegConfig.TTLCount;
  // i = 0...128 
  for (let i = 0; i < MAX_NUMBER_OF_CHANNELS; i++) {
    plotdata[i] = []; 
    // i = 0...16
    if (i < MAX_NUMBER_OF_CHANNELS_PER_PAGE) {
      av18[0].plotdata[i] = [];
      av18[1].plotdata[i] = [];
      av18[2].plotdata[i] = [];
      av18[3].plotdata[i] = [];
      av18[4].plotdata[i] = [];
      av18[5].plotdata[i] = [];
      av18[6].plotdata[i] = [];
      av18[7].plotdata[i] = [];
    }
    // i = 0...7
    if (i < MAX_NUMBER_OF_PAGES) {
      datasets18[i] = [];					// several datasets18 for 1 placeholder per page 
      plot_options18[i] = [];				// set of options for plot per each page 
      flot_placeholders18[i] = [];			// several placeholders: only one dataset per one placeholder
      plot18[i] = [];						// flot plots of any signals: 1  signal per 1 flot placeholder
      legends18[i] = [];					// legend area for any signals of plots 1-8
    }
  }

  RegConfig.AIChannelInfo = data.DeviceInfo.AIChannelInfo;
  RegConfig.AOChannelInfo = data.DeviceInfo.AOChannelInfo;
  RegConfig.DIChannelInfo = data.DeviceInfo.DIChannelInfo;
  RegConfig.DOChannelInfo = data.DeviceInfo.DOChannelInfo;

  const MI = 'MI';
  const FI = 'FI';
  const AI = 'AI';
  
  const AIChInfo_TYPES = {
    [AI]: 'АВ',
    [MI]: 'МВ',
    [FI]: 'ЧВ'
  };
  
  RegConfig.AIChannelInfo.forEach((item, index) => {
    const rusName = `${AIChInfo_TYPES[item.Type]} ${item.Index + 1}`;
    RegConfig.NameSignalsAI.push(rusName);
    RegConfig.NameSignalsAll.push(rusName);
  });

  for (let i = 0; i < data['DeviceInfo'].AOCount; i++) {
    // RegConfig.NameSignalsAll.push('��' + String(i + 1));
    // RegConfig.NameSignalsAO.push('��' + String(i + 1));
    RegConfig.NameSignalsAll.push(`АЕ ${i + 1}`);
    RegConfig.NameSignalsAO.push(`АЕ ${i + 1}`);
  }

  for (let i = 0; i < data['DeviceInfo'].DICount; i++) {
    // RegConfig.NameSignalsAll.push('��' + String(i + 1));
    // RegConfig.NameSignalsDI.push('��' + String(i + 1));
    RegConfig.NameSignalsAll.push(`ДВ ${i + 1}`);
    RegConfig.NameSignalsDI.push(`ДВ ${i + 1}`);
  }
  for (let i = 0; i < data['DeviceInfo'].DOCount; i++) {
    // RegConfig.NameSignalsAll.push('�' + String(i + 1));
    // RegConfig.NameSignalsDO.push('�' + String(i + 1));
    RegConfig.NameSignalsAll.push(`Р ${i + 1}`);
    RegConfig.NameSignalsDO.push(`Р ${i + 1}`);
  }

  for (let i = 0; i < data['DeviceInfo'].TTLCount; i++) {
    // RegConfig.NameSignalsAll.push('��' + String(i + 1));
    // RegConfig.NameSignalsTTL.push('��' + String(i + 1));
    RegConfig.NameSignalsAll.push(`Сумматор ${i + 1}`);
    RegConfig.NameSignalsTTL.push(`Сумматор ${i + 1}`);
  }

  // fill select lists
  $('<option value="0">нет</option>').appendTo($('#sel1'));
  $('<option value="0">нет</option>').appendTo($('#sel2'));
  $('<option value="0">нет</option>').appendTo($('#sel3'));
  $('<option value="0">нет</option>').appendTo($('#sel4'));
  $('<option value="0">нет</option>').appendTo($('#sel5'));
  $('<option value="0">нет</option>').appendTo($('#sel6'));
  $('<option value="0">нет</option>').appendTo($('#sel7'));
  $('<option value="0">нет</option>').appendTo($('#sel8'));
  RegConfig.NameSignalsAll.forEach((name,i) => {
  	$(`<option value=${i+1}>${name}</option>`).appendTo($('#sel1'));
  	$(`<option value=${i+1}>${name}</option>`).appendTo($('#sel2'));
  	$(`<option value=${i+1}>${name}</option>`).appendTo($('#sel3'));
  	$(`<option value=${i+1}>${name}</option>`).appendTo($('#sel4'));
  	$(`<option value=${i+1}>${name}</option>`).appendTo($('#sel5'));
  	$(`<option value=${i+1}>${name}</option>`).appendTo($('#sel6'));
  	$(`<option value=${i+1}>${name}</option>`).appendTo($('#sel7'));
  	$(`<option value=${i+1}>${name}</option>`).appendTo($('#sel8'));
  });
  // count rows number in the table
  rownumber = Math.max(
    RegConfig.AICount,
    RegConfig.AOCount,
    RegConfig.DICount,
    RegConfig.DOCount,
    RegConfig.TTLCount
  );
}

// set image in index.html file
function setRegImg() {
  if (typeof Storage === 'undefined') {
    console.warn("Your browser doesn't support localStorage");
    return;
  }
  const model = localStorage.getItem('ViER_model');
  let conf = model && model.split('-');
  if (!conf) return;
  const RegImg = document.getElementById('RegImg');
  const IMG_104k = '104K';
  const IMG_M7 = 'M7';
  const RegImages = {
    [IMG_104k]: 'img/104K.png',
    [IMG_M7]: 'img/M-7.png'
  };
  if (RegImages[conf[2]]) {
    RegImg.setAttribute('src', RegImages[conf[2]]);
  } else {
    RegImg.setAttribute('src', RegImages[IMG_104k]);
  }
}

function fillRegConfigChannels() {
  //AI	
  RegConfig.AIConfig.forEach(channel => {
    if (channel.Signal !== null) {
    	if(channel.Desc === null || channel.Desc === undefined){
    		RegConfig.DescSignalsAll.push("");
      		RegConfig.DescSignalsAI.push("");
    	}
    	else{
    		RegConfig.DescSignalsAll.push(channel.Desc);
      		RegConfig.DescSignalsAI.push(channel.Desc);
    	}
      	
      	if(channel.Units === null || channel.Units === undefined){
			RegConfig.UnitSignalsAll.push("");
      		RegConfig.UnitSignalsAI.push("");
      	}
      	else{
      		RegConfig.UnitSignalsAll.push(channel.Units);
      		RegConfig.UnitSignalsAI.push(channel.Units);
      	}
    } else {
      RegConfig.DescSignalsAll.push("");
      RegConfig.UnitSignalsAll.push("");
      RegConfig.DescSignalsAI.push("");
      RegConfig.UnitSignalsAI.push("");
    }
  });
  //AE
  RegConfig.AOConfig.forEach(channel => {
    RegConfig.UnitSignalsAll.push('');
    if(channel.Desc === null || channel.Desc === undefined){
    	RegConfig.DescSignalsAll.push("");
    	RegConfig.DescSignalsAO.push("");
    }
    else{
    	RegConfig.DescSignalsAll.push(channel.Desc);
    	RegConfig.DescSignalsAO.push(channel.Desc);
    }
  });
  //ДВ
  RegConfig.DIConfig.forEach(channel => {
    if (channel.Signal !== null) {
    	if(channel.Desc === null || channel.Desc === undefined){
    		RegConfig.DescSignalsAll.push("");
      		RegConfig.DescSignalsDI.push("");
    	}
      	else{
      		RegConfig.DescSignalsAll.push(channel.Desc);
      		RegConfig.DescSignalsDI.push(channel.Desc);
      	}
    } 
    else {
      RegConfig.DescSignalsAll.push("");
      RegConfig.DescSignalsDI.push("");
    }
    RegConfig.UnitSignalsAll.push("");
  });
  //Р
  RegConfig.DOConfig.forEach(channel => {
    RegConfig.UnitSignalsAll.push('');
    if(channel.Desc === null || channel.Desc === undefined){
    	RegConfig.DescSignalsAll.push("");
    	RegConfig.DescSignalsDO.push("");
    }
    else{
    	RegConfig.DescSignalsAll.push(channel.Desc);
    	RegConfig.DescSignalsDO.push(channel.Desc);
    }
  });
  //TTL
  RegConfig.TTLConfig.forEach(channel => {
    if(channel.Desc === null || channel.Desc === undefined){
    	RegConfig.DescSignalsAll.push("");
    	RegConfig.DescSignalsTTL.push("");
    }
    else{
    	RegConfig.DescSignalsAll.push(channel.Desc);
    	RegConfig.DescSignalsTTL.push(channel.Desc);
    }

    if(channel.Units === null || channel.Units === undefined){
    	RegConfig.UnitSignalsAll.push("");
    	RegConfig.UnitSignalsTTL.push("");
    }
    else{
    	RegConfig.UnitSignalsAll.push(channel.Units);
    	RegConfig.UnitSignalsTTL.push(channel.Units);
    }
  });
}

// get Reg Conf
async function getRegConfig() {
  const data = await fetchReg('/RegConfig');
  handleRegConfig(data);
}
// Handle Reg Conf
function handleRegConfig(data) {
  let tbls18 = []
  tbls18.push($('#tbl-1'));
  tbls18.push($('#tbl-2'));
  tbls18.push($('#tbl-3'));
  tbls18.push($('#tbl-4'));
  tbls18.push($('#tbl-5'));
  tbls18.push($('#tbl-6'));
  tbls18.push($('#tbl-7'));
  tbls18.push($('#tbl-8'));

  RegConfig.ConfigChangeCtr = data['ConfigChangeCtr'];
  RegConfig.AIConfig = data['AIConfig'];
  RegConfig.AOConfig = data['AOConfig'];
  RegConfig.DIConfig = data['DIConfig'];
  RegConfig.DOConfig = data['DOConfig'];
  RegConfig.TTLConfig = data['TTLConfig'];
  RegConfig.DisplayConfig = data['DisplayConfig'];

  fillRegConfigChannels();

  let PageTitles = [];
  PageTitles.push($('#Page1Title'));
  PageTitles.push($('#Page2Title'));
  PageTitles.push($('#Page3Title'));
  PageTitles.push($('#Page4Title'));
  PageTitles.push($('#Page5Title'));
  PageTitles.push($('#Page6Title'));
  PageTitles.push($('#Page7Title'));
  PageTitles.push($('#Page8Title'));
  // перебор страниц i - номер страницы 0...7
  RegConfig.DisplayConfig.Pages.forEach((page, i) => {
    PageTitles[i].html(page.Desc);
    av18[i].pageDesc = page.Desc;
    av18[i].npi = [];
    av18[i].vpi = [];
    av18[i].description = [];
    av18[i].units = [];
    av18[i].labels = [];
    legends18[i] = [];
    flot_placeholders18[i] = [];
    // перебор каналов на странице j - номер канала 0...15
    page.Channels.forEach( (channel,j) => {
      if (channel && channel !== 'null') {
        let type = channel.Source.Type;
        let index = channel.Source.Index;
        let vpi = channel.High;
        let npi = channel.Low;
        let description;
        let units;
        let name;
        const srcTypeHandlers = {
          [AI_TYPE]: () => {
            name = RegConfig.NameSignalsAI[index];
            description = RegConfig.AIConfig[index].Desc || '';
            units = RegConfig.AIConfig[index].Units || '';
          },
          [AO_TYPE]: () => {
            name = RegConfig.NameSignalsAO[index];
            description = RegConfig.AOConfig[index].Desc;
            units = '';
          },
          [DI_TYPE]: () => {
            name = RegConfig.NameSignalsDI[index];
            description = RegConfig.DIConfig[index].Desc || '';
            units = '';
          },
          [DO_TYPE]: () => {
            name = RegConfig.NameSignalsDO[index];
            description = RegConfig.DOConfig[index].Desc;
            units = '';
          },
          [TTL_TYPE]: () => {
            name = RegConfig.NameSignalsTTL[index];
            description = RegConfig.TTLConfig[index].Desc;
            units = RegConfig.TTLConfig[index].Units;
          }
        };

        srcTypeHandlers[type]();

        av18[i].labels.push(name);
        av18[i].npi.push(npi);
        av18[i].vpi.push(vpi);
        av18[i].description.push(description);
        av18[i].units.push(units);
        // fill divs flot_placeholders18 and legends18
        $( `<div style="width:100%;height:400px" id="flot_placeholders18_${i}_${j}"></div>` ).insertBefore(tbls18[i]); // div id = flot_placeholders18_0_0
        let tmp1 = $('#' + `flot_placeholders18_${i}_${j}`);
        flot_placeholders18[i].push(tmp1);

        $( `<div style="width:100%;height:30px" id="legends18_${i}_${j}"></div>` ).insertBefore(tbls18[i]);   // div id = legends18_0_0
        let tmp2 = $('#' + `legends18_${i}_${j}`);
        legends18[i].push(tmp2);

        // redraw plot 18 when dblclick
  // 		tmp1.dblclick(function() {
  // 			plot18[i][j] = $.plot(flot_placeholders18[i][j], datasets18[i][j], plot_options18[i][j]);
		//     plot18[i][j].setupGrid();
		//     plot18[i][j].draw();
		// });

      } // if (channel && channel !== 'null') {
    }); // channels
  });   // pages
}       // HandleRegConfig();


function isNumeric(n) {
  return !isNaN(parseFloat(n)) && isFinite(n);
}

async function getRegData() {
  curData = await fetchReg('/RegData');
  timerId = setTimeout(getRegData, PollPeriod * 1000);
  handleRegData(curData);
}

  



function handleRegData(data) {
  // clear all tables with data for 0-8 pages to update them
  // tables.forEach(table => {
  // 	table.empty();
  // });
 tables = [];
 tables.push($('#datatablebody-1'));
 tables.push($('#datatablebody-2'));
 tables.push($('#datatablebody-3'));
 tables.push($('#datatablebody-4'));	
 tables.push($('#datatablebody-5'));
 tables.push($('#datatablebody-6'));
 tables.push($('#datatablebody-7'));
 tables.push($('#datatablebody-8'));

  $('#datatablebody-0').empty();
  $('#datatablebody-1').empty();
  $('#datatablebody-2').empty();
  $('#datatablebody-3').empty();
  $('#datatablebody-4').empty();
  $('#datatablebody-5').empty();
  $('#datatablebody-6').empty();
  $('#datatablebody-7').empty();
  $('#datatablebody-8').empty();

  RegConfig.Status = data['Status'];
  if (RegConfig.ConfigChangeCtr !== data.ConfigChangeCtr) {
    getRegConfig();
  }

  time = data.Timestamp;
  //let temp = Math.round(new Date(time).getTime());
  timeUNIX = Date.parse(time);
  timestamps.push(timeUNIX);
  AIarray = data['AIData']; //JSON.parse(data['AIData']);
  AOarray = data['AOData']; //JSON.parse(data['AOData']);
  DIarray = data['DIData']; //data['DIData'].replace(/ |\[|\]/g, "").split(',');
  DOarray = data['DOData']; //data['DOData'].replace(/ |\[|\]/g, "").split(',');
  TTLarray = data['TTLData']; //JSON.parse(data['TTLData']);

  let k = 0;
  if(ShowGraph == 'true'){
	  // fill RegConfig with AI values and  avLabels[] with actual labels
	  for (let i = 0; i < RegConfig.AICount; i++) {
	    if (isNumeric(AIarray[i])) {
	      //av[k].push(AIarray[i]);
	      //avLabels.push(RegConfig.NameSignalsAI[i]);
	      plotdata[k++].push([timeUNIX, AIarray[i]]);
	    } else {
	      plotdata[k++].push([timeUNIX, 0.0]);
	    }
	  }
	  // fill RegConfig with AO values
	  for (let i = 0; i < RegConfig.AOCount; i++) {
	    if (isNumeric(AOarray[i])) {
	      //av[k].push(AOarray[i]);
	      //avLabels.push(RegConfig.NameSignalsAO[i]);
	      plotdata[k++].push([timeUNIX, AOarray[i]]);
	    } else {
	      plotdata[k++].push([timeUNIX, 0.0]);
	    }
	  }
	  // fill RegConfig with DI values
	  for (let i = 0; i < RegConfig.DICount; i++) {
	    if (isNumeric(DIarray[i])) {
	      //av[k].push(DIarray[i]);
	      //avLabels.push(RegConfig.NameSignalsDI[i]);
	      if(plotdata[k].length == 0){
	      	plotdata[k].push([timeUNIX, DIarray[i]]);
	      	k++;	
	      }
	      else {
	      	let t1 = plotdata[k][ plotdata[k].length-1 ] 
	        plotdata[k].push([timeUNIX, t1[1] ]);
	        plotdata[k].push([timeUNIX, DIarray[i] ]);	
	        k++;
	      }
	    } 
		else {
	      plotdata[k++].push([timeUNIX, 0.0]);
	    }
	  }
	  // fill RegConfig with DO values
	  for (let i = 0; i < RegConfig.DOCount; i++) {
	    if (isNumeric(DOarray[i])) {
	      //av[k].push(DOarray[i]);
	      //avLabels.push(RegConfig.NameSignalsDO[i]);
	      if(plotdata[k].length == 0){
	      	plotdata[k].push([timeUNIX, DOarray[i]]);
	      	k++;	
	      }
	      else {
	      	let t1 = plotdata[k][ plotdata[k].length-1 ] 
	        plotdata[k].push([timeUNIX, t1[1] ]);
	        plotdata[k].push([timeUNIX, DOarray[i] ]);	
	        k++;
	      }  
	    } 
	    else {
	      plotdata[k++].push([timeUNIX, 0.0]);
	    }
	  }
	  // fill RegConfig with TTL values
	  for (let i = 0; i < RegConfig.TTLCount; i++) {
	    if (isNumeric(TTLarray[i])) {
	      //av[k].push(TTLarray[i]);
	      //avLabels.push(RegConfig.NameSignalsTTL[i]);
	      plotdata[k++].push([timeUNIX, TTLarray[i]]);
	    } else {
	      plotdata[k++].push([timeUNIX, 0.0]);
	    }
	  }
  } // if(ShowGraph == 'true')
  // fill table 0 row by row
  for (let i = 0; i < rownumber; i++) {
    if (typeof AIarray[i] === 'undefined' || AIarray[i] === 'null') {
      AIarray[i] = '';
    }
    if (typeof AOarray[i] === 'undefined' || AOarray[i] === 'null') {
      AOarray[i] = '';
    }
    if (typeof DIarray[i] === 'undefined' || DIarray[i] === 'null') {
      DIarray[i] = '';
    }
    if (typeof DOarray[i] === 'undefined' || DOarray[i] === 'null') {
      DOarray[i] = '';
    }
    if (typeof TTLarray[i] === 'undefined' || TTLarray[i] === 'null') {
      TTLarray[i] = '';
    }

    rowTemplate_0 = `<tr>
    <td>${i + 1}</td>
    <td>${AIarray[i]}</td>
    <td>${
        !RegConfig.AIConfig[i] || RegConfig.AIConfig[i].Signal === 'null'
        ? ''
        : RegConfig.AIConfig[i].Units
    }</td>
    <td>${
        !RegConfig.AIConfig[i] || RegConfig.AIConfig[i].Signal === 'null'
        ? ''
        : RegConfig.AIConfig[i].Desc
    }</td>

    <td>${AOarray[i]}</td>
    <td>${RegConfig.AOConfig[i] ? RegConfig.AOConfig[i].Desc : ''}</td>

    <td>${DIarray[i]}</td>
    <td>${
        !RegConfig.DIConfig[i] || RegConfig.DIConfig[i].Signal === 'null'
        ? ''
        : RegConfig.DIConfig[i].Desc
    }</td>

    <td>${DOarray[i]}</td>
    <td>${RegConfig.DOConfig[i] ? RegConfig.DOConfig[i].Desc : ''}</td>

    <td>${TTLarray[i]}</td>
    <td>${RegConfig.TTLConfig[i] ? RegConfig.TTLConfig[i].Units : ''}</td>
    <td>${RegConfig.TTLConfig[i] ? RegConfig.TTLConfig[i].Desc : ''}</td>
    </tr>
    `;
    let $row = $(rowTemplate_0);

    $('#datatablebody-0').append($row);

    // color manipulation
    if (i % 2 == 0) {
      $row.addClass('evenrow');
    } else {
      $row.addClass('oddrow');
    }
    $row = null;
  } // for(let  i = 0; i < rownumber; i++)

  /*let av18 = [
            {npi:[], vpi:[], description:[], units:[], labels:[], plotdata:[], pageDesc},
            {npi:[], vpi:[], description:[], units:[], labels:[], plotdata:[], pageDesc},
            {npi:[], vpi:[], description:[], units:[], labels:[], plotdata:[], pageDesc},
            {npi:[], vpi:[], description:[], units:[], labels:[], plotdata:[], pageDesc},
            {npi:[], vpi:[], description:[], units:[], labels:[], plotdata:[], pageDesc},
            {npi:[], vpi:[], description:[], units:[], labels:[], plotdata:[], pageDesc},
            {npi:[], vpi:[], description:[], units:[], labels:[], plotdata:[], pageDesc},
            {npi:[], vpi:[], description:[], units:[], labels:[], plotdata:[], pageDesc},
    ];  */

  // process 1-8 pages
  // i - page number
  // k - number of plot in the page
  RegConfig.DisplayConfig.Pages.forEach((page, i) => {
    let table = tables[i];
    if(ShowGraph == 'true'){
    	datasets18[i] = [];
	}
    let k = 0;
    page.Channels.forEach(channel => {
      if (channel !== 'null') {
        let type = channel.Source.Type;
        let index = channel.Source.Index;
        let vpi = channel.High;
        let npi = channel.Low;
        let description = av18[i].description[k];
        let units = av18[i].units[k];
        let value;

        let typeHandlers = {
          [AI_TYPE]: () => {
            value = isNumeric(AIarray[i]) ? AIarray[index] : 0;
          },
          [AO_TYPE]: () => {
            value = isNumeric(AOarray[i]) ? AOarray[index] : 0;
          },
          [DI_TYPE]: () => {
            value = isNumeric(DIarray[i]) ? DIarray[index] : 0;
          },
          [DO_TYPE]: () => {
            value = isNumeric(DOarray[i]) ? DOarray[index] : 0;
          },
          [TTL_TYPE]: () => {
            value = isNumeric(TTLarray[i]) ? TTLarray[index] : 0;
          }
        };
        typeHandlers[type]();
        typeHandlers = null;
        
        if(ShowGraph == 'true'){
	        // form step for  discrete signals
	        if(type === DI_TYPE || type === DO_TYPE){
	          if(av18[i].plotdata[k].length == 0) {
	            av18[i].plotdata[k].push([timeUNIX, value]);
	          }
	          else{
	          	let t1 = av18[i].plotdata[k][ av18[i].plotdata[k].length-1 ] 
	            av18[i].plotdata[k].push([timeUNIX, t1[1] ]);
	            av18[i].plotdata[k].push([timeUNIX, value]);
	          }	
	        }
	        else{
	          av18[i].plotdata[k].push([timeUNIX, value]);
	        }

	        datasets18[i].push({
	          label: av18[i].labels[k],
	          data: av18[i].plotdata[k],
	          color: k,
	          points: { fillColor: '#FFFFFF', show: true },
	          page: i,
	          idx: k
	        });
	    } // if(ShowGraph == 'true')


        // fill tables 1-8 with online values
        rowTemplate_18 = `<tr>
        <td>${k + 1}</td>
        <td>${av18[i].labels[k] || ''}</td>
        <td>${value}</td>
        <td>${units}</td>
        <td>${description || ''}</td>
        <td>${(npi == '0') ? '0' : (npi || '')}</td>
        <td>${(vpi == '0') ? '0' : (vpi || '')}</td>
        </tr>`;
        let $rowjq = $(rowTemplate_18);
        table.append($rowjq);
        rowTemplate_18 = null;

        // color manipulation
        if (k % 2 == 0) {
          $rowjq.addClass('evenrow');
        } else {
          $rowjq.addClass('oddrow');
        }
        $rowjq = null;
        k++;
      }
    }); //page.Channels.forEach(channel => {
  }); // RegConfig.DisplayConfig.Pages.forEach((page, i) => {

  if(ShowGraph == 'true'){
  	// show all graphs
  	plotGraphic_0();
  	plotGraphics_18();
  }
  else{
  	// clear all graphs
  	$('#flot-0-placeholder').empty(); // clear canvas
  	for (let j = 0; j < MAX_NUMBER_OF_PAGES; j++) {
  		flot_placeholders18[j].forEach( (flot_placeholder,i) => {
  			flot_placeholder.empty();
  		});
  		legends18[j].forEach( (legend_placeholder,i) => {
  			legend_placeholder.empty();
  		});

  	}
  }
  plotTablo();
} // function handleRegData(data)

// get current time
async function getRegTime() {
  curTime = await fetchReg('/RegTime');
  handleRegTime(curTime);
  setTimeout(getRegTime, 1000);
}

function handleRegTime(data) {
  $('#time').text(data['time']);

  RegConfig.Status = data['Status'];
  if (RegConfig.Status == '0') {
    $('#status').text('OK');
  } else {
    $('#status').text('Ошибка № ' + RegConfig.Status);
  }

  if (RegConfig.ConfigChangeCtr != data['ConfigChangeCtr']) {
    getRegConfig();
  }
}
///////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////
//////////              PLOT         //////////////////////
///////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////
function plotGraphic_0() {
  plot_options0 = {
    series: {
      lines: { show: true },
      points: {
        radius: 1,
        fill: true,
        show: true
      }
    },
    axisLabels: {
      show: true
    },
    xaxis: {
      mode: 'time',
      timeformat: '%d-%m-%Y %H:%M:%S',
      timezone: 'browser',
      minTickSize: [30, 'second'],
      ticks: 3,
      position: 'bottom',
      axisLabel: 'Время',
      axisLabelUseCanvas: true,
      axisLabelFontSizePixels: 14,
      axisLabelFontFamily: 'Verdana, Arial',
      axisLabelPadding: 10
      //min: ( time - ?  :  )
    },
    yaxis: {
      axisLabel: 'Значение',
      axisLabelUseCanvas: true,
      axisLabelFontSizePixels: 14,
      axisLabelFontFamily: 'Verdana, Arial',
      axisLabelPadding: 10
    },
    /*
    legend: {
      container: $('#legend'),
      labelFormatter: (label, series) => {
        const cb = `<input  class="legendCB" type="checkbox" name="${
          series.idx
        }" 
        checked="${checkboxstates[series.idx] === 1 ? 'true' : 'false'}"
        onClick="togglePlot(${series.idx})" />
        ${label}
        ${
          !RegConfig.DescSignalsAll[series.idx]
            ? ''
            : ' - ' + RegConfig.DescSignalsAll[series.idx]
        }
        ${
          !RegConfig.UnitSignalsAll[series.idx]
            ? ''
            : ', ' + RegConfig.UnitSignalsAll[series.idx]
        }`;
        return cb;
      }
    },
    */
    grid: {
      hoverable: true,
      borderWidth: 3,
      mouseActiveRadius: 50,
      backgroundColor: { colors: ['#ffffff', '#EDF5FF'] },
      axisMargin: 20
    },
    selection: {
      mode: 'xy'
    }
  }; // plot_options0

/*  let dataset = {
      label: RegConfig.NameSignalsAll[0],
      data: plotdata[0],
      color: '#55555555',
      points: { fillColor: '#FFFFFF'}
    };
  
  final_datasets.forEach( final_dataset => {


  });
*/
  datasets2plot = [];
  final_datasets.forEach(plt => {
  	if(plt != null){
  		datasets2plot.push(plt);
  	}
  }); 

  if(plot0 == null){
  	plot0 = $.plot($('#flot-0-placeholder'), datasets2plot, plot_options0);
  }
  else{
  	plot0.setData(datasets2plot);
  	plot0.resize();
  	plot0.setupGrid();
  	plot0.draw();
  }

  $('#flot-0-placeholder').UseTooltip();

  // form plot window
  let minTime = plot0.getAxes().xaxis.min; //get plotted axis ranges
  let ShowInterval = localStorage.getItem('ShowInterval');
  if ((timeUNIX - minTime) / 1000 / 3600 > ShowInterval) {
    //delete 1-st element of plotdata[k][]
    plotdata.forEach(signal => {
      signal.shift();
    });
    let axes = plot0.getAxes();
    axes.xaxis.options.min = timeUNIX - ShowInterval * 3600 * 1000;
    plot0.setupGrid();
    plot0.draw();
  }
}

/////////////////////////////////////////////////////////
  /////////////////////////////////////////////////////////
  // plot graphics for pages 1-8
  ////////////////////////////////////////////////////////
  ////////////////////////////////////////////////////////
function plotGraphics_18() {
  let plot_options18_temp
  // j - number of page
  for (let j = 0; j < MAX_NUMBER_OF_PAGES; j++) {
    //try 
    //{
    // plot18[j] = [];	
    // plot_options18[j] = [];
    // i -number of signal on the page number j	
    datasets18[j].forEach((dataset,i) => {
      	// опции для сигналов
	    plot_options18_temp = {
	      series: {
	        lines: { show: true },
	        points: {
	          radius: 1,
	          fill: true,
	          show: true
	        }
	      },
	      xaxis: {
	        mode: 'time',
	        timeformat: '%d-%m-%Y %H:%M:%S',
	        timezone: 'browser',
	        minTickSize: [30, 'second'],
	        ticks: 3,
	        position: 'bottom',
	        axisLabel: 'Время',
	        axisLabelUseCanvas: true,
	        axisLabelFontSizePixels: 14,
	        axisLabelFontFamily: 'Verdana, Arial',
	        axisLabelPadding: 10
	      },
	      yaxis: {
	        axisLabel: av18[j].labels[i],
	        axisLabelUseCanvas: true,
	        axisLabelFontSizePixels: 14,
	        axisLabelFontFamily: 'Verdana, Arial',
	        axisLabelPadding: 10,
	        position: 'left',
      		min: ((av18[j].labels[i].indexOf('ДВ') > -1) || (av18[j].labels[i].indexOf('Р') > -1)) ? -0.5 : av18[j].npi[i],
      		max: ((av18[j].labels[i].indexOf('ДВ') > -1) || (av18[j].labels[i].indexOf('Р') > -1)) ?  1.5 : av18[j].vpi[i],
      		ticks: ((av18[j].labels[i].indexOf('ДВ') > -1) || (av18[j].labels[i].indexOf('Р') > -1)) ? 2 : 8
	      },
	      legend: {
	        container: legends18[j][i],
	        labelFormatter: function(label, series) {
	          let cb =
	            label +
	            (!av18[series.page].description[series.idx]
	              ? ''
	              : '-' + av18[series.page].description[series.idx]) +
	            (!av18[series.page].units[series.idx]
	              ? ''
	              : ',' + av18[series.page].units[series.idx]);
	          return cb;
	        }
	      },
	      grid: {
	        hoverable: true,
	        borderWidth: 3,
	        mouseActiveRadius: 50,
	        backgroundColor: { colors: ['#ffffff', '#EDF5FF'] },
	        axisMargin: 20
	      },
	      selection: {
	        mode: 'xy'
	      }
	    }; // plot_options18_temp
	    
	    // plot graph
	    let dataset_temp = [];
	    dataset_temp.push(dataset);	    
	    
	    if(plot18[j][i] == null){
		    let plot18_temp = $.plot(flot_placeholders18[j][i], dataset_temp, plot_options18_temp);
	      plot18[j].push(plot18_temp);
	      plot_options18[j].push(plot_options18_temp);
        plot18_temp.setupGrid();
	   	}
		  else{
		    plot18[j][i].setData(dataset_temp);
		    plot18[j][i].resize();
		    plot18[j][i].setupGrid();
		    plot18[j][i].draw();
        plot18[j][i].setupGrid();
		  }
		  flot_placeholders18[j][i].UseTooltip();
	    // zooming plots1-8
	    flot_placeholders18[j][i].bind('plotselected', function(event, ranges) {
		    $.each(plot18[j][i].getXAxes(), function(k, axis) {
		        let opts = axis.options;
		        opts.min = ranges.xaxis.from;
		        opts.max = ranges.xaxis.to;
		    });
		    
		    $.each(plot18[j][i].getYAxes(), function(k, axis) {
		        let opts = axis.options;
		        opts.min = ranges.yaxis.from;
		        opts.max = ranges.yaxis.to;
	        });
		    plot18[j][i].setupGrid();
		    plot18[j][i].clearSelection();
		    plot18[j][i].draw();
		  });

      // redraw plots1-8 when double click
      // flot_placeholders18[j][i].dblclick(function() {
      //   plot18[j][i] = $.plot(flot_placeholders18[j][i], datasets18[j][i], plot_options18[j][i]);
      //   plot18[j][i].draw();
      // });

	    // отображение именно выбранного интервала по времени
	    if (datasets18[j][i].length > 0) {
	        let minTime = plot18[j][i].getAxes().xaxis.min; //get plotted axis ranges
	        let ShowInterval = localStorage.getItem('ShowInterval');
	        if ((timeUNIX - minTime) / 1000 / 3600 > ShowInterval) {
	          //delete 1-st element of av18[i].plotdata[k]
	          $.each(av18, function(i, page) {
	            $.each(page.plotdata, function(j, signal) {
	              signal.shift();
	            });
	          });
	          let axes = plot18[j][i].getAxes();
	          axes.xaxis.options.min = timeUNIX - ShowInterval * 3600000;
	          plot18[j][i].setupGrid();
	          plot18[j][i].draw();
	        }
	    }

    }); // datasets18[j].forEach((dataset,i) => {
  } //for(let j=0; j < MAX_NUMBER_OF_PAGES; j++)         
} //plotGraphics_18();


/////////////////////////////////
// Tune Y axex
// function TuneYaxis(j) {
//   let yaxes_options = [];
//   for (let i = 0; i < av18[j].labels.length; i++) {
//     let temp = {
//       position: 'left',
//       color: 'black',
//       //tickFormatter: function (val, axis) {
//       //    return val + av18[j].units[i];
//       //},
//       axisLabel: av18[j].labels[i], // + " - " + av18[j].description[i] + (av18[j].units[i] ? ("," + av18[j].units[i]) : ""),
//       axisLabelUseCanvas: true,
//       axisLabelFontSizePixels: 14,
//       axisLabelFontFamily: 'Verdana, Arial',
//       axisLabelPadding: 10,
//       min: av18[j].npi[i],
//       max: av18[j].vpi[i],
//       ticks:
//         av18[j].labels[i].indexOf('ДВ') > -1 ||
//         av18[j].labels[i].indexOf('Р') > -1
//           ? 1
//           : 8
//     };
//     yaxes_options.push(temp);
//   }
//   return yaxes_options;
// }
///////////////////////////
/////////////////////////////////
/////////////////////////////////
//////           plotTablo  size = 8�4
/////////////////////////////////
/////////////////////////////////
function plotTablo() {
  let tablo = RegConfig.DisplayConfig.Tablo;
  let row = '';
  tablo.Channels.forEach((channel, i) => {
    if (i % 4 == 0) {
      row += '<tr>';
    }

    let typeHandlers = {
      [AI_TYPE]: () => `<td>
      <small>${RegConfig.NameSignalsAI[channel.Source.Index]}</small>
      <br />
      <font size="20">${AIarray[channel.Source.Index]}</font>
      <br />
      <small>${RegConfig.DescSignalsAI[channel.Source.Index]}</small>
      <br />
      <small>${channel.Low} - ${channel.High} ${
        RegConfig.UnitSignalsAI[channel.Source.Index]
      }</small>
      </td>`,

      [AO_TYPE]: () => `<td>
      <small>${RegConfig.NameSignalsAO[channel.Source.Index]}</small>
      <br />
      <font size="20">${AOarray[channel.Source.Index]}</font>
      <br />
      <small>${RegConfig.DescSignalsAO[channel.Source.Index]}</small>
      <br />
      <small>${channel.Low} - ${channel.High}</small>
      </td>`,

      [DI_TYPE]: () => `<td>
      <small>${RegConfig.NameSignalsDI[channel.Source.Index]}</small>
      <br />
      <font size="20">${DIarray[channel.Source.Index]}</font>
      <br />
      <small>${RegConfig.DescSignalsDI[channel.Source.Index]}</small>
      </td>`,

      [DO_TYPE]: () => `<td>
      <small>${RegConfig.NameSignalsDO[channel.Source.Index]}</small>
      <br />
      <font size="20">${DOarray[channel.Source.Index]}</font>
      <br />
      <small>${RegConfig.DescSignalsDO[channel.Source.Index]}</small>
      </td>`,

      [TTL_TYPE]: () => `<td>
      <small>${RegConfig.NameSignalsTTL[channel.Source.Index]}</small>
      <br />
      <font size="20">${TTLarray[channel.Source.Index]}</font>
      <br />
      <small>${RegConfig.DescSignalsTTL[channel.Source.Index]}</small>
      <br />
      <small>${channel.Low} - ${channel.High} ${
        RegConfig.UnitSignalsTTL[channel.Source.Index]
      }</small>
      </td>`
    };

    if (channel !== 'null') {
      row += typeHandlers[channel.Source.Type]();
    } else {
      row += '<td>-------</td>';
    }
    if ((i + 1) % 4 == 0) {
      row += '</tr>';
    }
    typeHandlers = null;
  }); // tablo.Channels.forEach((channel, i) => {
  $('#tablo').empty();
  $('#tablo').append(row);

  row = null;
  tablo = null;

}



////////////////////////////////
////////////////////////////////
////////////////////////////////


/*
function togglePlot(seriesIdx) {
  let previousPoint2 = plot.getData();
  previousPoint2[seriesIdx].points.show = previousPoint2[
    seriesIdx
  ].lines.show = !previousPoint2[seriesIdx].lines.show;
  plot.setData(previousPoint2);
  checkboxstates[seriesIdx] = !checkboxstates[seriesIdx];
  plot.draw();
  redrawCheckboxes();
}
*/

// tooltip function
let previousPoint = null,
  previousLabel = null;
$.fn.UseTooltip = function() {
  $(this).bind('plothover', function(event, pos, item) {
    if (item) {
      if (
        previousLabel != item.series.label ||
        previousPoint != item.dataIndex
      ) {
        previousPoint = item.dataIndex;
        previousLabel = item.series.label;
        $('#tooltip').remove();

        let x = item.datapoint[0];
        let y = item.datapoint[1];
        let date = new Date(x);
        let color = item.series.color;

        showTooltip(
          item.pageX,
          item.pageY,
          color,
          '<strong>' +
            item.series.label +
            '</strong><br>' +
            String(date.getDate()).padStart(2, '0') +
            '-' +
            String(date.getMonth() + 1).padStart(2, '0') +
            '-' +
            date.getFullYear() +
            ' ' +
            String(date.getHours()).padStart(2, '0') +
            ':' +
            String(date.getMinutes()).padStart(2, '0') +
            ':' +
            String(date.getSeconds()).padStart(2, '0') +
            ' : <strong>' +
            y +
            '</strong>'
        );
      }
    } else {
      $('#tooltip').remove();
      previousPoint = null;
    }
  });
};


function showTooltip(x, y, color, contents) {
  $('<div id="tooltip">' + contents + '</div>')
    .css({
      position: 'absolute',
      display: 'none',
      top: y - 40,
      left: x - 120,
      border: '2px solid ' + color,
      padding: '3px',
      'font-size': '9px',
      'border-radius': '5px',
      'background-color': '#fff',
      'font-family': 'Verdana, Arial, Helvetica, Tahoma, sans-serif',
      opacity: 0.9
    })
    .appendTo('body')
    .fadeIn(200);
}
/////////////////////////////////////////
function lengthOf(object) {
    var element_count = 0;
    for (e in object) { element_count++; }
    return element_count;
}