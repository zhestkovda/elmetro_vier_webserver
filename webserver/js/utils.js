export async function fetchData(endpoint, options = {}) {
  if (!options.method) {
    options.method = 'GET';
  }

  if (options.body && !(options.body instanceof FormData)) {
    options.headers = {
      'content-type': 'application/json;charset=UTF-8'
    };
    options.body = JSON.stringify(options.body);
  }

  const response = await call(endpoint, options);
  let responseData = null;

  try {
    responseData =
      options.method.toLowerCase() === 'delete' ||
      options.body instanceof FormData
        ? await response.text()
        : (await response.json()) || '';
  } catch (e) {
    console.warn(e);
  }

  if (response.status >= 300) {
    responseData.status = response.status;
    throw responseData;
  }

  return responseData;
}
