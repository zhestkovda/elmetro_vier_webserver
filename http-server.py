# -*- coding: utf-8 -*-
# Python 2.7
#!/usr/bin/env python

import cgi
from os import curdir, sep
import sys
if sys.version_info < (2, 8, 0):
    from BaseHTTPServer import BaseHTTPRequestHandler, HTTPServer
    from SimpleHTTPServer import SimpleHTTPRequestHandler
else:
    import http.server
import sys,time
#from requests.auth import HTTPBasicAuth
from urlparse import parse_qs
import json
import urlparse
import logging
import subprocess
import argparse
import numpy as np
import logging
#import cgitb; cgitb.enable()  ## This line enables CGI error reporting
import CGIHTTPServer
import cgitb
import threading
import webbrowser
import BaseHTTPServer
import SimpleHTTPServer
import random
import os
import base64
import math
import re
from datetime import datetime


HOST_NAME = 'localhost' # !!!REMEMBER TO CHANGE THIS!!!
PORT_NUMBER = 3000 # Maybe set this to 9000.
null = 'null'


class cViER():
    # ПАРАМЕТРЫ РЕГИСТРАТОРА
    isPassword = 1  # ''' 0001 .... 9999'''
    password = "1234"
    ViERType = 2
    # return 1 - ViER-104K
    # return 2 - ViER-M7
    # return 3 - ViER-104K* - small channel
    # return 4 - ViER-M7* - small channel
    # return 5 - ViER-104K-Ex
    ViERName = "ЭлМетро-ВиЭР-M-7-4АВ-ГП"
    ViERSerialNumber = 6606
    ViERFirmwareVersion = "2.1.128.56"
    ViERStatus = 0  # no errors
    ViERTime = "05-06-2017 14:22:12"
    ViERIPAdr = "127.0.0.1"
    ViERMask = "255.255.255.0"
    Slot1Type = 1
    Slot2Type = 1
    Slot3Type = 0
    Slot4Type = 0
    Slot5Type = 0
    Slot6Type = 8
    # return 0 - пустой слот
    # return 1 - ?
    # return 2 - ?
    # ........
    # return 20 - ?
    # return 21 - ?
    Slot1Values = 1
    Slot2Values = 1
    Slot3Values = 1
    Slot4Values = 1
    Slot5Values = 1
    Slot6Values = 1
    # struct Slot{
    #    int type;  - type of signals: return 1 - float for AE, AВ..; return 2 - bool (true=ON, false=OFF) for ДВ, Р..;
    #   int ChNumber; - number of channels max from 1 to 16
    #    double values[];
    #    string units[];
    #    string descriptions[];};
    AnalogVirtValues = 1
    DiscreteVirtValues = 1
    ViERSummators = 1
    ViERTimers = 1


    def getViERType(self):
        # return 1 - ViER-104K
        # return 2 - ViER-M7
        # return 3 - ViER-104K* - small channel
        # return 4 - ViER-M7* - small channel
        # return 5 - ViER-104K-Ex
        return self.ViERType

    def getPassword(self):
        return self.password

    def getViERName(self):
        return self.ViERName

    def getViERSerialNumber(self):
        return self.ViERSerialNumber

    def getViERFirmwareVersion(self):
        return self.ViERFirmwareVersion

    def getViERStatus(self):
        return self.ViERStatus

    def getViERTime(self):
        return self.ViERTime

    def getViERIPAdr(self):
        return self.ViERIPAdr

    def setViERIPAdr(self, ip):
        self.ViERIPAdr = ip

    def getViERMask(self):
        return self.ViERMask

    def setViERMask(self,mask):
        self.ViERMask = mask



class MyHandler(SimpleHTTPRequestHandler, cViER):
    def do_HEAD(self):
        #print "do_HEAD"
        self.send_response(200)
        self.send_header("Content-type", "text/html")
        self.end_headers()

    def do_AUTHHEAD(self):
        #print "do_AUTHHEAD"
        self.send_response(401)
        self.send_header('WWW-Authenticate', 'Basic realm=\"\"')
        self.send_header('Content-type', 'text/html')
        self.end_headers()

    def do_GET(self):
         #print urlparse.urlparse(self.path).path
         print (self.path)
         vier = cViER()
         if self.path.endswith(".ico"):
             f = open(curdir + sep + "webserver/favicon.ico")
             self.send_response(200)
             self.send_header('Content-type', 'image/x-icon')
             self.end_headers()
             self.wfile.write(f.read())
             f.close()
         else:
             pass #self.send_error(404, "File Not Found %s" % self.path)

         if self.path.endswith(".css"):
             f = open(curdir + sep + 'webserver'+ self.path)
             self.send_response(200)
             self.send_header('Content-type', 'text/css')
             self.end_headers()
             self.wfile.write(f.read())
             f.close()
         else:
             pass #self.send_error(404, "File Not Found %s" % self.path)

         if self.path.endswith(".png"):
             print ("send logo")
             self.send_response(200)
             self.send_header("Content-type", "image/png")
             statinfo = os.stat("webserver" + sep + urlparse.urlparse(self.path).path)
             img_size = statinfo.st_size
             self.send_header("Content-length", img_size)
             self.end_headers()
             f = open("webserver" + sep + urlparse.urlparse(self.path).path, 'rb')
             self.wfile.write(f.read())
             f.close()

         if self.path.endswith(".html"):
             f = open(curdir + sep + 'webserver/'+ self.path)
             self.send_response(200)
             self.send_header('Content-type', 'text/html')
             self.end_headers()
             self.wfile.write(f.read())
             f.close()
             return
         else:
             pass #self.send_error(404, "File Not Found %s" % self.path)

         if self.path.endswith(".js"):
             f = open(curdir + sep + 'webserver' + self.path)
             self.send_response(200)
             self.send_header('Content-type', 'application/javascript')
             self.end_headers()
             self.wfile.write(f.read())
             f.close()
             return
         else:
             pass #self.send_error(404, "File Not Found %s" % self.path)

         # ONCE
         if self.path.endswith("RegInfo"):
             # send JSON with reg info
             self.send_response(200)
             self.send_header('Content-Type', 'application/json')
             response = json.dumps({
                  "version": 1,
                  "manufacturer": "Elmetro",
                  "model": "Elmetro-VR",
                  "configuration": "Elmetro-VR-M7-XX-XX",
                  "serial": 14, "swversion": "2.1.150.0",
                  "DeviceInfo": {
                      "AICount": 60, "AOCount": 4, "DICount": 32, "DOCount": 16, "IPCount": 0, "TTLCount": 16,
                      "DisplayPageCount": 8, "MaxChannelPerDisplayPage": 16, "DisplayPageTablo": 'true',
                      "DisplayPageTabloColCount": 4, "DisplayPageTabloRowCount": 8,
                      "AIPhysSignalSupport": [{"Type": "none"}, {"Type": "Current_mA"}, {"Type": "Voltage_V"},
                                              {"Type": "Voltage_mV"}, {"Type": "Resistance_Ohm"}, {"Type": "Pyrometer"},
                                              {"Type": "Thermocouple"}, {"Type": "Thermistor"}, {"Type": "Frequency"},
                                              {"Type": "Counter"}, {"Type": "Math"}, {"Type": "Flow"},
                                              {"Type": "Serial"}, {"Type": "CAN"}, {"Type": "External"}],
                      "AIChannelInfo": [{"Type": "AI", "Index": 0, "Name": "\u0410\u04121"},
                                        {"Type": "AI", "Index": 1, "Name": "\u0410\u04122"},
                                        {"Type": "AI", "Index": 2, "Name": "\u0410\u04123"},
                                        {"Type": "AI", "Index": 3, "Name": "\u0410\u04124"},
                                        {"Type": "AI", "Index": 4, "Name": "\u0410\u04125"},
                                        {"Type": "AI", "Index": 5, "Name": "\u0410\u04126"},
                                        {"Type": "AI", "Index": 6, "Name": "\u0410\u04127"},
                                        {"Type": "AI", "Index": 7, "Name": "\u0410\u04128"},
                                        {"Type": "FI", "Index": 0, "Name": "\u0427\u04121"},
                                        {"Type": "FI", "Index": 1, "Name": "\u0427\u04122"},
                                        {"Type": "FI", "Index": 2, "Name": "\u0427\u04123"},
                                        {"Type": "FI", "Index": 3, "Name": "\u0427\u04124"},
                                        {"Type": "FI", "Index": 4, "Name": "\u0427\u04125"},
                                        {"Type": "FI", "Index": 5, "Name": "\u0427\u04126"},
                                        {"Type": "FI", "Index": 6, "Name": "\u0427\u04127"},
                                        {"Type": "FI", "Index": 7, "Name": "\u0427\u04128"},
                                        {"Type": "MI", "Index": 0, "Name": "\u041c\u04121"},
                                        {"Type": "MI", "Index": 1, "Name": "\u041c\u04122"},
                                        {"Type": "MI", "Index": 2, "Name": "\u041c\u04123"},
                                        {"Type": "MI", "Index": 3, "Name": "\u041c\u04124"},
                                        {"Type": "MI", "Index": 4, "Name": "\u041c\u04125"},
                                        {"Type": "MI", "Index": 5, "Name": "\u041c\u04126"},
                                        {"Type": "MI", "Index": 6, "Name": "\u041c\u04127"},
                                        {"Type": "MI", "Index": 7, "Name": "\u041c\u04128"},
                                        {"Type": "MI", "Index": 8, "Name": "\u041c\u04129"},
                                        {"Type": "MI", "Index": 9, "Name": "\u041c\u041210"},
                                        {"Type": "MI", "Index": 10, "Name": "\u041c\u041211"},
                                        {"Type": "MI", "Index": 11, "Name": "\u041c\u041212"},
                                        {"Type": "MI", "Index": 12, "Name": "\u041c\u041213"},
                                        {"Type": "MI", "Index": 13, "Name": "\u041c\u041214"},
                                        {"Type": "MI", "Index": 14, "Name": "\u041c\u041215"},
                                        {"Type": "MI", "Index": 15, "Name": "\u041c\u041216"},
                                        {"Type": "MI", "Index": 16, "Name": "\u041c\u041217"},
                                        {"Type": "MI", "Index": 17, "Name": "\u041c\u041218"},
                                        {"Type": "MI", "Index": 18, "Name": "\u041c\u041219"},
                                        {"Type": "MI", "Index": 19, "Name": "\u041c\u041220"},
                                        {"Type": "MI", "Index": 20, "Name": "\u041c\u041221"},
                                        {"Type": "MI", "Index": 21, "Name": "\u041c\u041222"},
                                        {"Type": "MI", "Index": 22, "Name": "\u041c\u041223"},
                                        {"Type": "MI", "Index": 23, "Name": "\u041c\u041224"},
                                        {"Type": "MI", "Index": 24, "Name": "\u041c\u041225"},
                                        {"Type": "MI", "Index": 25, "Name": "\u041c\u041226"},
                                        {"Type": "MI", "Index": 26, "Name": "\u041c\u041227"},
                                        {"Type": "MI", "Index": 27, "Name": "\u041c\u041228"},
                                        {"Type": "MI", "Index": 28, "Name": "\u041c\u041229"},
                                        {"Type": "MI", "Index": 29, "Name": "\u041c\u041230"},
                                        {"Type": "MI", "Index": 30, "Name": "\u041c\u041231"},
                                        {"Type": "MI", "Index": 31, "Name": "\u041c\u041232"},
                                        {"Type": "MI", "Index": 32, "Name": "\u041c\u041233"},
                                        {"Type": "MI", "Index": 33, "Name": "\u041c\u041234"},
                                        {"Type": "MI", "Index": 34, "Name": "\u041c\u041235"},
                                        {"Type": "MI", "Index": 35, "Name": "\u041c\u041236"},
                                        {"Type": "MI", "Index": 36, "Name": "\u041c\u041237"},
                                        {"Type": "MI", "Index": 37, "Name": "\u041c\u041238"},
                                        {"Type": "MI", "Index": 38, "Name": "\u041c\u041239"},
                                        {"Type": "MI", "Index": 39, "Name": "\u041c\u041240"},
                                        {"Type": "MI", "Index": 40, "Name": "\u041c\u041241"},
                                        {"Type": "MI", "Index": 41, "Name": "\u041c\u041242"},
                                        {"Type": "MI", "Index": 42, "Name": "\u041c\u041243"},
                                        {"Type": "MI", "Index": 43, "Name": "\u041c\u041244"}],
                      "AOChannelInfo": [{"Type": "AE", "Index": 0, "Name": "\u0410\u04151"},
                                        {"Type": "AE", "Index": 1, "Name": "\u0410\u04152"},
                                        {"Type": "AE", "Index": 2, "Name": "\u0410\u04153"},
                                        {"Type": "AE", "Index": 3, "Name": "\u0410\u04154"}],
                      "DIChannelInfo": [{"Name": "\u0414\u04121"}, {"Name": "\u0414\u04122"}, {"Name": "\u0414\u04123"},
                                        {"Name": "\u0414\u04124"}, {"Name": "\u0414\u04125"}, {"Name": "\u0414\u04126"},
                                        {"Name": "\u0414\u04127"}, {"Name": "\u0414\u04128"}, {"Name": "\u0414\u04129"},
                                        {"Name": "\u0414\u041210"}, {"Name": "\u0414\u041211"},
                                        {"Name": "\u0414\u041212"}, {"Name": "\u0414\u041213"},
                                        {"Name": "\u0414\u041214"}, {"Name": "\u0414\u041215"},
                                        {"Name": "\u0414\u041216"}, {"Name": "\u0414\u041217"},
                                        {"Name": "\u0414\u041218"}, {"Name": "\u0414\u041219"},
                                        {"Name": "\u0414\u041220"}, {"Name": "\u0414\u041221"},
                                        {"Name": "\u0414\u041222"}, {"Name": "\u0414\u041223"},
                                        {"Name": "\u0414\u041224"}, {"Name": "\u0414\u041225"},
                                        {"Name": "\u0414\u041226"}, {"Name": "\u0414\u041227"},
                                        {"Name": "\u0414\u041228"}, {"Name": "\u0414\u041229"},
                                        {"Name": "\u0414\u041230"}, {"Name": "\u0414\u041231"},
                                        {"Name": "\u0414\u041232"}],
                      "DOChannelInfo": [{"Type": "R", "Name": "\u04201"}, {"Type": "R", "Name": "\u04202"},
                                        {"Type": "R", "Name": "\u04203"}, {"Type": "R", "Name": "\u04204"},
                                        {"Type": "R", "Name": "\u04205"}, {"Type": "R", "Name": "\u04206"},
                                        {"Type": "R", "Name": "\u04207"}, {"Type": "R", "Name": "\u04208"},
                                        {"Type": "R", "Name": "\u04209"}, {"Type": "R", "Name": "\u042010"},
                                        {"Type": "R", "Name": "\u042011"}, {"Type": "R", "Name": "\u042012"},
                                        {"Type": "R", "Name": "\u042013"}, {"Type": "R", "Name": "\u042014"},
                                        {"Type": "R", "Name": "\u042015"}, {"Type": "R", "Name": "\u042016"}]
                  }
             })
             self.end_headers()
             print (response)
             self.wfile.write(response)
             return

         #ONCE
         if self.path.endswith("RegConfig"):
             # send JSON with reg info
             self.send_response(200)
             self.send_header('Content-Type', 'application/json')
             response = json.dumps({
                  "ConfigChangeCtr": 0,
                  "AIConfig": [{"Signal": {"Index": 1}, "Desc": "сигнал АВ1", "Units": "mm"},
                               {"Signal": null}, {"Signal": null}, {"Signal": null}, {"Signal": null}, {"Signal": null},
                               {"Signal": null}, {"Signal": null}, {"Signal": null}, {"Signal": null}, {"Signal": null},
                               {"Signal": null}, {"Signal": null}, {"Signal": null}, {"Signal": null}, {"Signal": null},
                               {"Signal": {"Index": 1}, "Desc": "температура", "Units": "grad C"},
                               {"Signal": {"Index": 1}, "Desc": "Давление", "Units": "Pa"},
                               {"Signal": {"Index": 1}, "Desc": "Перепад давления", "Units": "Pa"},
                               {"Signal": {"Index": 1}, "Desc": "", "Units": ""},
                               {"Signal": {"Index": 2}, "Desc": "", "Units": ""},
                               {"Signal": null},{"Signal": null}, {"Signal": null}, {"Signal": null}, {"Signal": null},
                               {"Signal": {"Index": 1}, "Desc": "сигнал МВ11", "Units": "кг"},
                               {"Signal": {"Index": 1}, "Desc": "сигнал МВ12", "Units": "мм"},
                               {"Signal": null},
                               {"Signal": null},
                               {"Signal": null}, {"Signal": null}, {"Signal": null}, {"Signal": null}, {"Signal": null},
                               {"Signal": null}, {"Signal": null}, {"Signal": null}, {"Signal": null}, {"Signal": null},
                               {"Signal": null}, {"Signal": null}, {"Signal": null}, {"Signal": null}, {"Signal": null},
                               {"Signal": null}, {"Signal": null}, {"Signal": null}, {"Signal": null}, {"Signal": null},
                               {"Signal": null}, {"Signal": null}, {"Signal": null}, {"Signal": null}, {"Signal": null},
                               {"Signal": null}, {"Signal": null}, {"Signal": null}, {"Signal": null},
                               {"Signal": null}],
                  "AOConfig": [{"Control": {"Index": 1, "Name": "math"}, "Desc": "аналог выход 1"},
                               {"Control": {"Index": 1, "Name": "math"}, "Desc": "аналог выход 2"},
                               {"Control": {"Index": 1, "Name": "math"}, "Desc": "аналог выход 3"},
                               {"Control": {"Index": 1, "Name": "math"}, "Desc": "аналог выход 4"}],
                  "DIConfig": [{"Signal": {"Index": 1}, "Desc": "дискретный вход 1"}, {"Signal": {"Index": 1}, "Desc": "дискретный вход 2"},
                               {"Signal": null}, {"Signal": null}, {"Signal": null}, {"Signal": null}, {"Signal": null},
                               {"Signal": null}, {"Signal": {"Index": 4}, "Desc": "ДВ9"},
                               {"Signal": {"Index": 4}, "Desc": "ДВ10"}, {"Signal": null}, {"Signal": null},
                               {"Signal": null}, {"Signal": null}, {"Signal": null}, {"Signal": null}, {"Signal": null},
                               {"Signal": null}, {"Signal": null}, {"Signal": null}, {"Signal": null}, {"Signal": null},
                               {"Signal": null}, {"Signal": null}, {"Signal": null}, {"Signal": null}, {"Signal": null},
                               {"Signal": null}, {"Signal": null}, {"Signal": null}, {"Signal": null}, {"Signal": null}],
                  "DOConfig": [{"Control": {"Type": "event"}, "Desc": "Р1"}, {"Control": {"Type": "math"}, "Desc": "Р2"},
                               {"Control": {"Type": "math"}, "Desc": "Р3"}, {"Control": {"Type": "math"}, "Desc": "Р4"},
                               {"Control": {"Type": "math"}, "Desc": "Р5"}, {"Control": {"Type": "math"}, "Desc": "Р6"},
                               {"Control": {"Type": "math"}, "Desc": "Р7"}, {"Control": {"Type": "math"}, "Desc": "Р8"},
                               {"Control": {"Type": "math"}, "Desc": "Р9"}, {"Control": {"Type": "math"}, "Desc": "Р10"},
                               {"Control": {"Type": "math"}, "Desc": "Р11"}, {"Control": {"Type": "math"}, "Desc": "Р12"},
                               {"Control": {"Type": "math"}, "Desc": "Р13"}, {"Control": {"Type": "math"}, "Desc": "Р14"},
                               {"Control": {"Type": "math"}, "Desc": "Р15"}, {"Control": {"Type": "math"}, "Desc": "Р16"}],
                  "TTLConfig": [{"Source": null, "Desc": "сумматор  1", "Units": "кг"},
                                {"Source": null, "Desc": "сумматор  2", "Units": "т"}, {"Source": null, "Desc": "сумматор  3", "Units": "т"},
                                {"Source": null, "Desc": "сумматор  4", "Units": "т"}, {"Source": null, "Desc": "сумматор  5", "Units": "т"},
                                {"Source": null, "Desc": "сумматор  6", "Units": "т"}, {"Source": null, "Desc": "сумматор  7", "Units": "т"},
                                {"Source": null, "Desc": "сумматор  8", "Units": "т"}, {"Source": null, "Desc": "сумматор  9", "Units": "т"},
                                {"Source": null, "Desc": "сумматор  10", "Units": "т"}, {"Source": null, "Desc": "сумматор  11", "Units": "т"},
                                {"Source": null, "Desc": "сумматор  12", "Units": "т"}, {"Source": null, "Desc": "сумматор  13", "Units": "т"},
                                {"Source": null, "Desc": "сумматор  14", "Units": "т"}, {"Source": null, "Desc": "", "Units": ""},
                                {"Source": null, "Desc": "сумматор  16", "Units": "м3"}],
                  "DisplayConfig": {
                      "Pages": [
                          {"ViewMode": "trend", "TrendMode": "combined", "Direction": "horizontal", "Color": "black",
                           "TickCount": 0, "Scale": 1, "Desc": "Page1",
                           "Channels": [
                               {"Source": {"Type": "AI", "Index": 26}, "Color": "0000FF", "Low": -10, "High": 10,
                                "Width": 1},
                               {"Source": {"Type": "AO", "Index": 0}, "Color": "00FF00", "Low": 0, "High": 60,
                                "Width": 1},
                               {"Source": {"Type": "AI", "Index": 0}, "Color": "00FFFF", "Low": 0, "High": 70,
                                "Width": 1},
                               null,
                               null,
                               {"Source": {"Type": "DI", "Index": 0}, "Color": "FFFF00"},
                               {"Source": {"Type": "DI", "Index": 1}, "Color": "2AA52A"}, null, null, null, null, null,
                               null, null, null, null]},
                          {"ViewMode": "trend", "TrendMode": "combined", "Direction": "horizontal", "Color": "black",
                           "TickCount": 0, "Scale": 1, "Desc": "Page2",
                           "Channels": [
                               {"Source": {"Type": "AI", "Index": 16}, "Color": "000080", "Low": 0, "High": 60,
                                "Width": 1},
                               {"Source": {"Type": "AI", "Index": 17}, "Color": "800000", "Low": 0, "High": 20,
                                "Width": 1},
                               {"Source": {"Type": "AI", "Index": 18}, "Color": "404040", "Low": 0, "High": 10,
                                "Width": 1},
                               {"Source": {"Type": "AI", "Index": 19}, "Color": "800080", "Low": 0, "High": 10,
                                "Width": 1},
                               {"Source": {"Type": "AI", "Index": 20}, "Color": "808060", "Low": 0, "High": 10,
                                "Width": 1}, null, null, null, null, null, null, null, null, null, null, null]},
                          {"ViewMode": "trend", "TrendMode": "combined", "Direction": "horizontal", "Color": "black",
                           "TickCount": 0, "Scale": 1, "Desc": "Page3",
                           "Channels": [null, null, null, null, null, null, null, null, null, null, null, null, null,
                                        null, null, null]},
                          {"ViewMode": "trend", "TrendMode": "combined", "Direction": "horizontal", "Color": "black",
                           "TickCount": 0, "Scale": 1, "Desc": "Page4",
                           "Channels": [null, null, null, null, null, null, null, null, null, null, null, null, null,
                                        null, null, null]},
                          {"ViewMode": "trend", "TrendMode": "combined", "Direction": "horizontal", "Color": "black",
                           "TickCount": 0, "Scale": 1, "Desc": "Page5",
                           "Channels": [null, null, null, null, null, null, null, null, null, null, null, null, null,
                                        null, null, null]},
                          {"ViewMode": "trend", "TrendMode": "combined", "Direction": "horizontal", "Color": "black",
                           "TickCount": 0, "Scale": 1, "Desc": "Page6",
                           "Channels": [null, null, null, null, null, null, null, null, null, null, null, null, null,
                                        null, null, null]},
                          {"ViewMode": "trend", "TrendMode": "combined", "Direction": "horizontal", "Color": "black",
                           "TickCount": 0, "Scale": 1, "Desc": "Page7",
                           "Channels": [null, null, null, null, null, null, null, null, null, null, null, null, null,
                                        null, null, null]},
                          {"ViewMode": "trend", "TrendMode": "combined", "Direction": "horizontal", "Color": "black",
                           "TickCount": 0, "Scale": 1, "Desc": "Page8",
                           "Channels": [null, null, null, null, null, null, null, null, null, null, null, null, null,
                                        null, null, null]}],
                      "Tablo": {"Color": "black", "Desc": "",
                                "Channels": [
                               {"Source": {"Type": "AI", "Index": 26}, "Color": "0000FF", "Low": 0, "High": 10,
                                "Width": 1},
                               {"Source": {"Type": "AO", "Index": 2}, "Color": "00FF00", "Low": 0, "High": 70,
                                "Width": 1},
                               {"Source": {"Type": "AI", "Index": 0}, "Color": "00FFFF", "Low": 0, "High": 10,
                                "Width": 1},
                               null,
                               null,
                               {"Source": {"Type": "DI", "Index": 0}, "Color": "FFFF00"},
                               {"Source": {"Type": "DI", "Index": 1}, "Color": "2AA52A"},
                               null, null, null, null, null,null, null, null, null,
                               null, null, null, null, null, null, null, null,
                               null, null, null, null, null, null, null, null ]}
                  }
             })
             self.end_headers()
             print (response)
             self.wfile.write(response)
             return


         #PERIODICALLY
         if self.path.endswith("RegData"):
             # send JSON with reg measurements
             #f = open(curdir + sep + "webserver/RegData.json")
             self.send_response(200)
             self.send_header('Content-Type', 'application/json')
             self.end_headers()
             #temp = round(random.uniform(0,100),2)
             #print temp

             response = json.dumps({'Timestamp': datetime.now().strftime('%Y/%m/%d %H:%M:%S'),
                                    'ConfigChangeCtr': 0,
                                    'Status': 0,
                                    'AIData': (np.round(np.random.uniform(low=0, high=10, size=(59)),2).tolist() + ['null']),
                                    'AOData': (np.round(np.random.uniform(low=10, high=20, size=(3)),2).tolist() + ['null']),
                                    #'AOData': [math.sin(time[ind])] + [math.sin(ind) + 1] + [math.sin(ind) + 2] + [math.sin(ind) + 3],
                                    'DIData': (np.random.choice([0, 1],31).tolist() + ['null']),
                                    'DOData': (np.random.choice([0, 1],15).tolist() + ['null']),
                                    'TTLData':(np.round(np.random.uniform(low=40, high=50, size=(15)), 2).tolist() + ['null'])
                                    })
             print (response)
             self.wfile.write(response)
             #f.close()
             return

         #PERIODICALLY
         if self.path.endswith("RegTime"):
             # send current time in format "05-06-2017 14:22:12"
             self.send_response(200)
             self.send_header('Content-Type', 'application/json')
             self.end_headers()
             # {"time":"2018/03/23 17:43:58","ConfigChangeCtr":0,"Status":0}
             response = json.dumps({'time': datetime.now().strftime('%Y/%m/%d %H:%M:%S'),
                                    'ConfigChangeCtr': 0,
                                    'Status': 0
                                    })
             self.wfile.write(response)
             return

         if self.headers.getheader('Authorization') == None:
             self.do_AUTHHEAD()
             #self.wfile.write('no auth header received')
             pass
         else:
             temp = self.headers.getheader('Authorization').split()
             #print temp[1]
             password = base64.b64decode(temp[1])
             #print password
             if password == "admin:" + vier.getPassword():
                 #self.do_HEAD()
                 #self.wfile.write(self.headers.getheader('Authorization'))
                 #self.wfile.write('authenticated!')
                 f = open(curdir + sep + "webserver/index.html")
                 self.send_response(200)
                 self.send_header("Content-type", "text/html")
                 self.end_headers()
                 self.wfile.write(f.read())
                 f.close()
                 pass
             else:
                 self.do_AUTHHEAD()
                 #self.wfile.write(self.headers.getheader('Authorization'))
                 self.wfile.write('Error login or password!')
                 pass


         '''
         if urlparse.urlparse(self.path).path == 'data.json':
             f = open(curdir + sep + "/data.json")
             self.send_response(200)
             self.send_header("Content-type", "image/x-icon")
             self.end_headers()
             self.wfile.write(f.read())
             f.close()

         if urlparse.urlparse(self.path).path == '/favicon.ico':
             f = open(curdir + sep + "/favicon.ico")
             self.send_response(200)
             self.send_header("Content-type", "image/x-icon")
             self.end_headers()
             self.wfile.write(f.read())
             f.close()

         # send CSS file on request
         if urlparse.urlparse(self.path).path == '/ws.css':
             f = open(curdir + sep + "/ws.css")
             self.send_response(200)
             self.send_header("Content-type", "text/css")
             self.end_headers()
             self.wfile.write(f.read())
             f.close()

         
         # send index file on request
         if urlparse.urlparse(self.path).path == '/index.html':
             f = open(curdir + sep + "/index.html")
             self.send_response(200)
             self.send_header("Content-type", "text/html")
             self.end_headers()
             self.wfile.write(f.read())
             f.close()

         # send angular file on request
         if urlparse.urlparse(self.path).path == '/angular.min.js':
             f = open(curdir + sep + "/angular.min.js")
             self.send_response(200)
             self.send_header("Content-type", "text/html")
             self.end_headers()
             self.wfile.write(f.read())
             f.close()

         # send jquery file on request
         if urlparse.urlparse(self.path).path == '/jquery-3.2.1.js':
             f = open(curdir + sep + "/jquery-3.2.1.js")
             self.send_response(200)
             self.send_header("Content-type", "text/html")
             self.end_headers()
             self.wfile.write(f.read())
             f.close()
         '''

    def do_POST(self):
        logging.info("POST request")
        print (urlparse.urlparse(self.path).path)
        ''' 
        self.send_response(200)
        self.end_headers()
        try:
             ctype, pdict = cgi.parse_header(self.headers.getheader("content-type"))
             if ctype == "multipart/form-data":
                 query = cgi.parse_multipart(self.rfile, pdict)
                 query.values()
             self.send_response(200)
             self.end_headers()
        except Exception:
             pass
        '''
def main():
    logging.info('Start the HTTT server')
    server = HTTPServer((HOST_NAME, PORT_NUMBER), MyHandler)
    print (time.asctime(), "Server Starts - %s:%s" % (HOST_NAME, PORT_NUMBER))
    try:
        server.serve_forever()
    except KeyboardInterrupt:
        pass
    server.server_close()
    print (time.asctime(), "Server Stops - %s:%s" % (HOST_NAME, PORT_NUMBER))

if __name__ == "__main__":
    main()
